<?php

namespace TheFeed\Controleur;

use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\Conteneur;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ControleurGenerique {

    protected static function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    
    protected static function rediriger(string $nomRoute, array $params = []) : RedirectResponse
    {
        $generetor = Conteneur::recupererService("ServiceGenerateur");
        return new RedirectResponse($generetor->generate($nomRoute,$params));
    }

    public static function afficherErreur($errorMessage = "", $statusCode = 400): Response
    {
        $reponse = ControleurGenerique::afficherVue('vueGenerale.php', [
            "pagetitle" => "Problème",
            "cheminVueBody" => "erreur.php",
            "errorMessage" => $errorMessage
        ]);
    
        $reponse->setStatusCode($statusCode);
        return $reponse;
    }

}